<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionConsulta1(){
		// select * from emple;
		$consulta= Emple::find()->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 1',
                         "texto"=>'Mostrar todos los empleados',
			"datos"=>$consulta,
		]);
	
	}

        public function actionConsulta2(){
		// select  apellido, oficio from emple;
                $consulta= Emple::find()
                ->select([ 'apellido','oficio'])
                ->all();

		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 2',
                         "texto"=>'Mostrar  apellidos y oficio de los empleados',
			"datos"=>$consulta,
		]);
	
	}
        
         public function actionConsulta3(){
		// select  count(*) from emple;
                $consulta= Emple::find()
                ->count();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 3',
                         "texto"=>'Cuenta empleados',
			"datos"=>$consulta,
		]);
	
	}
        
         public function actionConsulta4(){
		// select * from emple order by apellido;
                $consulta= Emple::find()
                ->orderBy('apellido')
                        ->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 4',
                         "texto"=>'Empleados ordenados por apellido de forma ascendente',
			"datos"=>$consulta,
		]);
	
	}
        
         public function actionConsulta5(){
		// select * from emple order by apellido desc;
                $consulta= Emple::find()
                ->orderBy(['apellido'=> SORT_DESC])
                        ->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 5',
                         "texto"=>'Empleados ordenados por apellido de forma descendente',
			"datos"=>$consulta,
		]);
	
	}
        
          public function actionConsulta6(){
		// select * from emple order by dept_no desc;
                $consulta= Emple::find()
                ->orderBy(['dept_no'=> SORT_DESC])
                        ->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 6',
                         "texto"=>'Empleados ordenados por dept_no de forma descendente',
			"datos"=>$consulta,
		]);
	
	}
        
        public function actionConsulta7(){
		// select * from emple order by dept_no desc and oficio asc;
                $consulta= Emple::find()
                ->orderBy(['dept_no'=> SORT_DESC,
                           'oficio'=> SORT_ASC])
                        ->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 7',
                         "texto"=>'Empleados ordenados por dept_no de forma descendente y oficio ascendente',
			"datos"=>$consulta,
		]);
	
	}
        
         public function actionConsulta8(){
		// select * from emple order by dept_no desc and apellido asc;
                $consulta= Emple::find()
                ->orderBy(['dept_no'=> SORT_DESC,
                           'apellido'=> SORT_ASC])
                        ->all();
		
		return $this->render('consultas',[
                         "titulo"=>'Consulta 8',
                         "texto"=>'Empleados ordenados por dept_no de forma descendente y apellido ascendente',
			"datos"=>$consulta,
		]);
	
	}
        
         public function actionConsulta9(){
		// select emp_no from emple where salario>2000;
                
                $consulta= Emple:: find()
                        ->select('emp_no')
                        ->where (
                             'salario>2000')
                   ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 9',
                         "texto"=>'Mostrar empleados de empelados  con salario mayor a 2000',
			"datos"=>$consulta,
                        
		]);
	}
        
             public function actionConsulta10(){
		// select emp_no, apellido from emple where salario<2000;
                
                $consulta= Emple:: find()
                        ->select('emp_no','apellido')
                        ->where (
                             'salario<2000')
                   ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 10',
                         "texto"=>'Mostrar emp_no y apellido de empelados con salario menor a 2000',
			"datos"=>$consulta,
                        
		]);
	}
        
        
            public function actionConsulta11(){
		// select *from emple where salario between 1500 and 2500;
                
                $consulta= Emple:: find()
                        ->where (
                             'salario between 1500 and 2500')
                   ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 11',
                         "texto"=>'Mostrar empleados con salario between 1500 and 2500',
			"datos"=>$consulta,
                        
		]);
	}
        
         public function actionConsulta12(){
		// select * from emple where oficio='Analista';
                
                $consulta= Emple::find()
                        ->where(['like', 'oficio','analista'])
                   ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 12',
                         "texto"=>'Mostrar empleados cuyo oficio sea analista',
			"datos"=>$consulta,
                        
		]);
	}
        
        
         public function actionConsulta13(){
		// select * from emple where oficio=analista y salario >2000;
                $consulta= Emple:: find()->
                   where(['and', 
                       ['like', 'oficio','analista'],
                           'salario>2000'])
                        ->all();
                
		return $this->render('consultas',[
                         "titulo"=>'Consulta 13',
                         "texto"=>'Mostrar empleados cuyo oficio sea analista y salario >2000',
			"datos"=>$consulta,
		]);
          }
	
        
	
        public function actionConsulta14(){
		// select apellido, oficio from emple where dept_no=20 ;
                
                $consulta= Emple:: find()->select('apellido','oficio')->
                        where('dept_no=20')
                        ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 14',
                         "texto"=>'Mostrar apellidos y oficios de empleados de departamento nº 20',
			"datos"=>$consulta,
                        
		]);
	
	}
        
         public function actionConsulta15(){
		// select count(*) from emple where oficio=vendedor ;
                
                $consulta= Emple:: find()->
                        where(['like', 'oficio','vendedor'])
                        ->count();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 15',
                         "texto"=>'Cuenta empleados cuyo oficio es vendedor',
			"datos"=>$consulta,
                        
		]);
	
	}
        
          public function actionConsulta16(){
		// select * from emple where apellido= like 'n% or m%' order by apellido asc ;
                
                $consulta= Emple:: find()->
                        where(["or","left(apellido,1)='M'","left(apellido,1)='N'"])
                        ->orderBy(['apellido'=> SORT_ASC])
                        ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 16',
                         "texto"=>'Mostrar empleados cuyo apellido comience por m o n y ordenar por apellido de forma asc',
			"datos"=>$consulta,
                        
		]);
	
	}
        
         public function actionConsulta17(){
		// select * from emple where oficio like 'vendedor' order by apellido asc ;
                
                $consulta= Emple:: find()->
                        where(['like', 'oficio','vendedor'])
                        ->orderBy(['apellido'=> SORT_ASC])
                        ->all();
                    
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 17',
                         "texto"=>'Mostrar empleados cuyo oficio sea vendedor ordenar por apellido de forma asc',
			"datos"=>$consulta,
                        
		]);
	
	}
    
          public function actionConsulta18(){
		// select * from emple where oficio like 'vendedor' order by apellido asc ;
                
                $max= Emple:: find()
                              //->select('apellido')
                              ->max('salario')
                              ;
                    
                $consulta= Emple::find()
                        ->select('apellido')
                        ->where('salario='.$max)
                        ->one();
                
                
                
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 18',
                         "texto"=>'Mostrar el apellido del empleado que más gana',
			"datos"=>$consulta,
                        
		]);
	
	}
    
        
          public function actionConsulta19(){
		// select * from emple where oficio like 'vendedor' order by apellido asc ;
                $consulta= Emple:: find()->
                   where(['and', 
                       ['like', 'oficio','analista'],
                           'dept_no=10'])
                        ->orderBy(['apellido'=> SORT_ASC],
                                        ['oficio'=> SORT_ASC])
                        ->all();
                
		return $this->render('consultas',[
                         "titulo"=>'Consulta 19',
                         "texto"=>'Mostrar los empleados cuyo departamento sea el 10 y cuyo oficio analista ordenar por apellido y oficio de forma asc',
			"datos"=>$consulta,
		]);
          }
	
              public function actionConsulta20(){
		// select * from emple  ;
                $consulta= Emple:: find()->
                      select('month(fecha_alt)') 
                             ->all();
                
		return $this->render('consultas',[
                         "titulo"=>'Consulta 20',
                         "texto"=>'Mostrar los meses en que los empelados se han dado de alta',
			"datos"=>$consulta,
		]);
          }
          
               public function actionConsulta21(){
		// select * from emple  ;
                $consulta= Emple:: find()->
                      select('year(fecha_alt)') 
                             ->all();
                
		return $this->render('consultas',[
                         "titulo"=>'Consulta 21',
                         "texto"=>'Mostrar los años en que los empelados se han dado de alta',
			"datos"=>$consulta,
		]);
          }
          
             public function actionConsulta22(){
		// select * from emple  ;
                $consulta= Emple:: find()->
                      select('dayofmonth(fecha_alt)') 
                             ->all();
                
		return $this->render('consultas',[
                         "titulo"=>'Consulta 22',
                         "texto"=>'Mostrar los dias del mese en que los empleados se han dado de alta',
			"datos"=>$consulta,
		]);
          }
          
            public function actionConsulta23(){
                
                $consulta= Emple:: find()
                        ->select('apellido')
                        ->where ([
                            'or',
                             'dept_no=20',
                            'salario>2000'])
                   
                        ->all();
               
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 23',
                         "texto"=>'Mostrar los apellidos de los empleados con salario mayor a 2000 o pertenezcan al departamento nº20',
			"datos"=>$consulta,
		]);
	
	}
        
           public function actionConsulta24(){
                
                $consulta= Emple:: find()
                        ->select(['apellido','depart.dnombre'])
                        ->joinWith(['deptNo'])
                        ->all();
               
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 24',
                         "texto"=>'Mostrar los apellidos de los empelados con el nombre del departamento al que pertenece',
			"datos"=>$consulta,
		]);
	
	}
        
        
            public function actionConsulta25(){
                
                $consulta= Emple:: find()
                        ->select('apellido','oficio','dnombre')
                        ->joinWith(['deptNo'])
                        ->all();
               
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 25',
                         "texto"=>'Mostrar los apellidos y oficio de los empleados con el nombre del departamento al que pertenece',
			"datos"=>$consulta,
		]);
	
	} 
        
              public function actionConsulta28(){
                
                 $consulta= Emple:: find()->
                        select('apellido')
                        ->orderBy(['oficio'=> SORT_ASC],
                                   ['apellido'=> SORT_ASC])
                        ->all();
               
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 28',
                         "texto"=>'Mostrar los apellidos de los empleados ordenados por oficio y apellido',
			"datos"=>$consulta,
		]);
	
	} 
        
             public function actionConsulta29(){
                
                  $consulta= Emple:: find()
                       ->select('apellido')
                       -> where("left(apellido,1)='A'")
                        ->all();
               
              		return $this->render('consultas',[
                         "titulo"=>'Consulta 29',
                         "texto"=>'Mostrar los apellidos de los empleados cuyo apellido comience por a',
			"datos"=>$consulta,
		]);
	
	} 
            
             public function actionConsulta30(){
                        $consulta= Emple:: find()->
                         select('apellido')
                        ->where(["or","left(apellido,1)='A'","left(apellido,1)='M'"])
                        
                        ->all();
                        
                        return $this->render('consultas',[
                         "titulo"=>'Consulta 30',
                         "texto"=>'Mostrar los apellidos de los empleados cuyo apellido comience por a o por m',
			"datos"=>$consulta,
		]);
	
	} 
        
                public function actionConsulta31(){
                        $consulta= Emple:: find()->
                         select('apellido')
                        ->where("right(apellido,1)!='Z'")
                        ->orderBy(['apellido'=> SORT_ASC])
                        ->all();
                        
                        return $this->render('consultas',[
                         "titulo"=>'Consulta 31',
                         "texto"=>'Mostrar los apellidos de los empleados cuyo apellido no termine por z',
			"datos"=>$consulta,
		]);
	
	} 
        
        /* public function actionConsulta6(){
                
                $consulta= Emple:: find()
                        ->select('dept_no')
                        ->where ([
                            'or',
                            'salario>1500',
                            //['>','salario'=>1500]
                            ['like', 'apellido','s%']
                        ])
                   
                        ->all();
                var_dump($consulta);
                exit;
              		
	
	}*/
    
        
       // consultas con data provider
        
           public function actionConsultadp1(){
                
                $consulta= Emple:: find();
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp1',[
                   "dataprovider"=>$a,
		]);
	
	}
        
            public function actionConsultadp2(){
                
                $consulta= Emple:: find()
                        ->select(['apellido','oficio']);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp2',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
            public function actionConsultadp3(){
                
                $consulta= Emple:: find()
                        ->select('count(*) as salida' );
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp3',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
        public function actionConsultadp4(){
                
                $consulta= Emple:: find()
                 ->orderBy(['apellido'=> SORT_ASC]);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp4',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
        
          public function actionConsultadp5(){
                
                $consulta= Emple:: find()
                 ->orderBy(['apellido'=> SORT_DESC]);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp5',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
         public function actionConsultadp6(){
                
                $consulta= Emple:: find()
                 ->orderBy(['dept_no'=> SORT_DESC]);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp6',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
         public function actionConsultadp7(){
                
                $consulta= Emple:: find()
                 ->orderBy(['dept_no'=> SORT_DESC,
                            'oficio'=> SORT_ASC]
                           );
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp7',[
                   "dataprovider"=>$a,
                
		]);
	
	}
      
        
          public function actionConsultadp8(){
                
                $consulta= Emple:: find()
                 ->orderBy(['dept_no'=> SORT_DESC,
                            'apellido'=> SORT_ASC]
                           );
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp8',[
                   "dataprovider"=>$a,
                
		]);
	
	}
        
       public function actionConsultadp9(){
                
                $consulta= Emple:: find()
                 ->select('emp_no')
                   ->where('salario>2000');
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp9',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
          public function actionConsultadp10(){
                
                $consulta= Emple:: find()
                 ->select('emp_no')
                   ->where('salario<2000');
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp10',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
         public function actionConsultadp11(){
                
                $consulta= Emple:: find()
                  ->where('salario between 1500 and 2500');
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp11',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
        
         public function actionConsultadp12(){
                
                $consulta= Emple:: find()
                  ->where(['like','oficio','analista']);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp12',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
            public function actionConsultadp13(){
                
                $consulta= Emple:: find()
                  ->where(['and', 
                       ['like', 'oficio','analista'],
                           'salario>2000']);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp13',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
        
        public function actionConsultadp14(){
                
                $consulta= Emple:: find()
                  ->where('dept_no=20');
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp14',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
        public function actionConsultadp15(){
                
                $consulta= Emple:: find()
                  ->select('count(*) as salida')      
                  ->where(['like','oficio','vendedor']);
                
                $a= new ActiveDataProvider([
                    "query"=>$consulta,
                ]);
                        
               
              	return $this->render('consultasdp15',[
                   "dataprovider"=>$a,
                
		]);
	
	} 
        
        
        
         /* public function actionConsulta24(){
        $consulta = new Query;
        $consulta->select([
        'Emple.apellido AS nombre', 
        'Depart.dnombre as  departamento']
        )  
        ->from('Depart')
        ->join('LEFT OUTER JOIN', 'Depart.dnombre',
            'depart.dept_no =emple.dept_no')		
      
        ->LIMIT(5)	; 
		
        $command = $consulta->createCommand();
        $data = $consulta->queryAll();

        return $this->render('consultas',[
                         "titulo"=>'Consulta 24',
                         "texto"=>'Mostrar los apellidos de los empelados con el nombre del departamento al que pertenece',
			"datos"=>$consulta,
		]);
	
	}*/
     
    
    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   
    public function actionDelete($id){
    
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
