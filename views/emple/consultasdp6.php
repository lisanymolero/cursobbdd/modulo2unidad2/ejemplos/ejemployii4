<?php

/* @var $this yii\web\View */
    use yii\helpers\Html;
    use yii\grid\GridView;


    $this->title = 'Consulta6s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataprovider,
        'columns' => [
            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',
           
        ],
    ]); ?>
    
    <div>

