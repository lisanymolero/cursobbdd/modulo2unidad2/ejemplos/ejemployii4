<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Empleados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

               'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            //'salario',
            //'comision',
            //'dept_no',
            'salario',
            'comision',
            'dept_no',
            'deptNo.dnombre',
            'deptNo.loc',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
