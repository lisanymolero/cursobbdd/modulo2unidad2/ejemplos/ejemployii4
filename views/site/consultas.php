<?php

/* @var $this yii\web\View */
    use yii\helpers\Html;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de seleccion</h1>

        <p class="lead">Realizando consultas</p>
    </div>

    <div class="body-content">

       
            <div class="container">
     
             <div class="col-sm-4">
                <h2>Consulta1</h2>

                <p>Mostrar todos los registros de empelados</p>

                <p><?= Html::a("ejecutar consulta", ['emple/consulta1'],['class'=> 'btn btn-default']) ?></p>
            </div>
                
                
            <div class="col-lg-12">
                <h2>Consulta2</h2>

                <p>Mostrar  apellidos y oficio de los empleados</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta2']) ?></p>

            </div>
                
              
            <div class="col-lg-12">
                <h2>Consulta3</h2>
                    <p>Cuenta numero de empelados</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta3']) ?></p>

        
            </div>
             <div class="col-lg-12">
                <h2>Consulta4</h2>

                <p>Empleados ordenados por apellido de forma ascendente</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta4']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta5</h2>

                <p>Empleados ordenados por apellido de forma descendente</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta5']) ?></p>

            </div>
             <div class="col-lg-12">
                <h2>Consulta6</h2>

                <p>Empleados ordenados por dept_no de forma descendente</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta6']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta7</h2>

                <p>Empleados ordenados por dept_no de forma desc y oficio asc</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta7']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta8</h2>

                <p>Empleados ordenados por dept_no de forma desc y apellido asc</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta8']) ?></p>

            </div>
            
            
            <div class="col-lg-12">
                <h2>Consulta9</h2>

                <p>Mostrar empleados de departamentos  con salario mayor a 2000</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta9']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta10</h2>

                <p>Mostrar empleados de departamentos  con salario menor a 2000</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta10']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta11</h2>

                <p>Mostrar empleados con salario entre 1500 y 2500</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta11']) ?></p>

            </div>
           
            
            <div class="col-lg-12">
                <h2>Consulta12</h2>

                <p>Mostrar empleados cuyo oficio sea analista</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta12']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta13</h2>

                <p>Mostrar empleados cuyo oficio sea analista y salario mayor a 2000</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta13']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta14</h2>

                <p>Mostrar apellido y oficio de empleados del departamento nº 20</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta14']) ?></p>

            </div>
            
            
            <div class="col-lg-12">
                <h2>Consulta15</h2>

                <p>Cuenta empleados cuyo oficio es vendedor</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta15']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta16</h2>

                <p>Mostrar empleados cuyo apellido comience por m o n y ordenar por apellido de forma asc</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta16']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta17</h2>

                <p>Mostrar empleados cuyo oficio sea vendedor y ordenar por apellido de forma asc</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta17']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta18</h2>

                <p>Mostrar el apellido del empleado que más gana</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta18']) ?></p>

            </div>
            
            
             <div class="col-lg-12">
                <h2>Consulta19</h2>

                <p>Mostrar los empleados cuyo departamento sea el 10 y cuyo oficio analista ordenar por apellido y oficio de forma asc</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta19']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta20</h2>

                <p>Mostrar los meses en que los empelados se han dado de alta</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta20']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta21</h2>

                <p>Mostrar los años en que los empelados se han dado de alta</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta21']) ?></p>

            </div>
            
            
            <div class="col-lg-12">
                <h2>Consulta22</h2>

                <p>Mostrar los dias del mes en que los empelados se han dado de alta</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta22']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>Consulta23</h2>

                <p>Mostrar los apellidos de los empleados con salario mayor a 2000 o pertenezcan al departamento nº20</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta23']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta24</h2>

                <p>Mostrar los apellidos de los empelados con el nombre del departamento al que pertenece</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta24']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta25</h2>

                <p>Mostrar los apellidos y oficio de los empleados con el nombre del departamento al que pertenece</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta25']) ?></p>

            </div>
            
            
             <div class="col-lg-12">
                <h2>Consulta28</h2>

                <p>Mostrar los apellidos de los empleados ordenados por oficio y apellido</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta28']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta29</h2>

                <p>Mostrar los apellidos de los empleados cuyo apellido comience por a</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta29']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta30</h2>

                <p>Mostrar los apellidos de los empleados cuyo apellido comience por a o por m</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta30']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>Consulta31</h2>

                <p>Mostrar los apellidos de los empleados cuyo apellido no termine por z</p>
                <p><?= Html::a("ejecutar consulta", ['emple/consulta31']) ?></p>

            </div>
            
             <div class="col-lg-12">
                <h2>C1</h2>

                <p>Mostrar todos los registros de departamento</p>
                <p><?= Html::a("ejecutar consulta", ['depart/c1']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>C2</h2>

                <p>Mostrar dept_no y localización de departamento</p>
                <p><?= Html::a("ejecutar consulta", ['depart/c2']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>C3</h2>

                <p>Mostrar dept_no, nombre y localización de departamento</p>
                <p><?= Html::a("ejecutar consulta", ['depart/c3']) ?></p>

            </div>
            
            <div class="col-lg-12">
                <h2>C4</h2>

                <p>Cuenta departamentos</p>
                <p><?= Html::a("ejecutar consulta", ['depart/c4']) ?></p>

            </div>
        </div>

    </div>
</div>
